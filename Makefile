SRC := $(wildcard src/*.cpp) $(wildcard Util/*.cpp) $(wildcard Net/*.cpp) $(wildcard Net/Socket/*.cpp)
OBJS := $(patsubst %.cpp, %.o, $(SRC))
CXX = g++
CXXFLAGS = -I . -ggdb3 -Wall
LDFLAGS = -lssl -lcrypto -lpthread
OUT = dropletc

BUILD : $(OBJS)
	$(CXX) -o $(OUT) $(OBJS) $(LDFLAGS)
COMPILE : $(OBJS)
CLEAN:
	rm -rf $(OBJS)
ALL : BUILD CLEAN
