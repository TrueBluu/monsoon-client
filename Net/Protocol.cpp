#include <string.h>
#include <arpa/inet.h>
#include <Util/Util.hpp>
#include "Protocol.hpp"

//TODO Add more checks here
int AddFilePacket::parse()
{
  if (header_size_raw.size() < 4)
    return 1;

  header_size = ntohl(*(uint32_t*)header_size_raw.data());

  //There isn't enough data in the header, return 1 for error
  if (header.size() < header_size)
    return 1;

  //copy ecc key to header, at the correct offset
  memcpy(ECC_PUB, header.data() + 2, 32);
  //Get file name size
  uint32_t fnamesize = ntohl(*(uint32_t*)(header.data() + 34));
  //Get file name
  fname = std::string(reinterpret_cast<char const*>(header.data() + 38), fnamesize);
  //Data
  data = std::vector<unsigned char>(io_data);
  //Size of data
  data_size = ntohl(*(uint32_t*)(header.data() + 38 + fnamesize));

  return 0;
}

void pushback_bytes(std::vector<unsigned char>& v, const unsigned char* bytes, uint32_t size)
{
  for (unsigned int i = 0; i < size; i++)
  {
    v.push_back(bytes[i]);
  }
}

//TODO add some checks here
//Like checking if ECC_PUB is nullptr
int AddFilePacket::g_raw()
{
  //clear vector in case there was data in it already
  header.clear();

  //Add opcode to packet
  //htonl because all integers are sent with network order endianness (big endian)
  uint16_t op_bytes = htonl(op);
  pushback_bytes(header, (const unsigned char*)&op_bytes, 2);
  //

  //Add ecc key to packet
  pushback_bytes(header, ECC_PUB, 32);

  //File name length
  uint32_t fnamesize = htonl(fname.size());
  pushback_bytes(header, (const unsigned char*)&fnamesize, 4);

  //File name
  pushback_bytes(header, reinterpret_cast<const unsigned char*>(fname.c_str()), fname.size());

  //Size of the file data, in network order
  uint32_t data_size_net = htonl(data.size());
  pushback_bytes(header, (const unsigned char*)&data_size_net, 4);

  //copy data to output vector
  io_data = std::vector<unsigned char>(data);

  //set the size field for convenience
  header_size = header.size();

  //clear header_size in case there was something in there already
  header_size_raw.clear();
  //All numbers are sent in network byte order
  uint32_t net_size = htonl(header_size);

  //Add the bytes
  pushback_bytes(header_size_raw, (unsigned char*)&net_size, 4);

  return 0;
}
