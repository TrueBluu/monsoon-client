## What is monsoon?

Monsoon is a decentralized file storage and retrieval protocol. It aims to fix the problems of similar applications like [Filecoin](https://filecoin.io/) and to provide a convenient interface for users.
