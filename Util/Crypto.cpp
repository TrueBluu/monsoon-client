#include <openssl/evp.h>
#include "Crypto.hpp"


std::vector<unsigned char> Crypto::SHA512(const unsigned char* d, size_t s)
{
  //SHA512 digest is 64 bytes
  std::vector<unsigned char> digest(64);
  EVP_MD_CTX* ctx = EVP_MD_CTX_create();

  EVP_DigestInit_ex(ctx, EVP_sha512(), NULL);
  EVP_DigestUpdate(ctx, d, s);
  EVP_DigestFinal_ex(ctx, digest.data(), NULL);
  EVP_MD_CTX_free(ctx);

  return digest;
}
