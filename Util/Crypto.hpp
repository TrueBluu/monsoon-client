#pragma once
#include <stddef.h>
#include <vector>

struct Crypto
{
  static std::vector<unsigned char> SHA512(const unsigned char* d, size_t s);
};
