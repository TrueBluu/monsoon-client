#include "Util.hpp"

bool IsSafeStr(std::string str)
{
  for (unsigned int i = 0; i < str.length(); i++)
  {
    if (str[i] == '/' || str[i] == '\\' || str[i] == '~')
      return 0;
  }

  return 1;
}
