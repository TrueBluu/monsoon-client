#pragma once
#include <iostream>

#define LOGGING

#ifdef LOGGING
  #define LOG_ERROR(message) std::cerr << "[LOG][ERROR] " << message;
  #define LOG_INFO(message) std::cout << "[LOG][INFO] " << message;
  #define LOG_WARNING(message) std::cerr << "[LOG][WARNING] " << message;
#endif

#ifndef LOGGING
  #define LOG_ERROR(message)
  #define LOG_INFO(message)
  #define LOG_WARNING(message)
#endif

bool IsSafeStr(std::string str);
