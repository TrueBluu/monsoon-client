#include <Net/Socket/TcpConnection.hpp>
#include <iostream>
#include <string.h>
#include <thread>
#include <Net/Protocol.hpp>
#include <arpa/inet.h>

int main()
{
  AddFilePacket pack(0, reinterpret_cast<const unsigned char*>(std::string("afdafdafadsf").c_str()), "test.txt", {'a','b','c','d'});
  TcpConnection conn("127.0.0.1",40000);

  if (conn.m_failure())
  {
    std::cout << "Failure\n";
    pack.clean();
    return -1;
  }

  pack.g_raw();
  conn.s_send(pack.header_size_raw.data(), 4);
  conn.wait_for_sig(PROTOCOL_ACK);
  conn.s_send(pack.header.data(), pack.header.size());
  conn.wait_for_sig(PROTOCOL_ACK);
  conn.s_send(pack.data.data(), pack.data.size());

  conn.s_close();

  pack.clean();


  return 0;
}
